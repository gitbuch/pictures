FROM node:12
WORKDIR /src
COPY package*.json /src/
RUN npm i --production
COPY server/ /src/server/
COPY frontend/ /src/frontend/
CMD npm start
